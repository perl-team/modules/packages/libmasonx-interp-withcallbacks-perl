libmasonx-interp-withcallbacks-perl (1.20-1) unstable; urgency=medium

  * Import upstream version 1.20.
  * Drop Attempt_to_call_undefined_import_method_with_arguments.patch,
    merged upstream.
  * Update years of upstream copyright.
  * Drop test dependency on libtest-pod-perl (test removed).

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Aug 2024 21:03:38 +0200

libmasonx-interp-withcallbacks-perl (1.19-5) unstable; urgency=medium

  * Add patch to fix a warning with perl 5.40. (Closes: #1078114)
  * Update years of packaging copyright.
  * Update alternative build dependencies.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 10 Aug 2024 18:37:56 +0200

libmasonx-interp-withcallbacks-perl (1.19-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Fix day-of-week for changelog entry 1.13-3.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 16:24:02 +0100

libmasonx-interp-withcallbacks-perl (1.19-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 14:00:01 +0100

libmasonx-interp-withcallbacks-perl (1.19-3) unstable; urgency=medium

  * Add debian/upstream/metadata.
  * Add explicit build dependency on libcgi-pm-perl.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 17 Jun 2015 23:33:38 +0200

libmasonx-interp-withcallbacks-perl (1.19-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Declare the package autopkgtestable
  * Update to Standards-Version 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * Update my email address

 -- Niko Tyni <ntyni@debian.org>  Sat, 06 Jun 2015 11:13:17 +0300

libmasonx-interp-withcallbacks-perl (1.19-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Fabrizio Regalli ]
  * Switch to DEP5 license format.
  * Add myself to Uploaders.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Email change: gregor herrmann -> gregoa@debian.org
  * New upstream release

  [ gregor herrmann ]
  * Remove alternative (build) dependencies that are already satisfied
    in oldstable.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Florian Schlichting ]
  * Bump Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Remove version from (build-)dependencies satisfied in oldstable.
  * Remove markup from long description.
  * Update years and name/email of upstream copyright.
  * Add myself to Uploaders and copyright.
  * Remove mod_perl from Build-Depends-Indep and overrides from debian/rules:
    The test suite was written for Apache 1, produces lots of ugly warnings
    but still won't run with Apache2.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Mon, 09 Apr 2012 21:10:54 +0200

libmasonx-interp-withcallbacks-perl (1.18-1) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/watch: use dist-based URL.

  * New upstream release.
  * debian/watch: extended regexp for matching upstream releases.
  * debian/copyright: use author-independent upstream source URL; adjust
    years of copyright.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/control:
    - change my email address and wrap long lines
    - add Suggests: libapache2-mod-perl2 and Depends: libhtml-mason-perl (>=
      1:1.23), libparams-callbackrequest-perl (>= 1.15),
      libclass-container-perl (>= 0.09).
  * Refresh debian/rules, no functional changes (except for dropping README,
    which is just a text version of the POD documentation).

 -- gregor herrmann <gregoa@debian.org>  Fri, 09 May 2008 17:07:34 +0200

libmasonx-interp-withcallbacks-perl (1.17-1) unstable; urgency=low

  * New upstream release with the t/htdocs/dhandler patch from 1.16-2.
  * Revert the libhtml-mason-perl build-dependency bump since this
    works with earlier versions of HTML::Mason too.

 -- Niko Tyni <ntyni@iki.fi>  Wed, 25 Jul 2007 21:44:09 +0300

libmasonx-interp-withcallbacks-perl (1.16-2) unstable; urgency=medium

  [ Niko Tyni ]
  * t/htdocs/dhandler now returns 200 (HTTP OK) to fix test failures
    with HTML::Mason 1.36. (Closes: #434445)
  * Urgency set to medium because of an RC bug fix.

  [ Damyan Ivanov ]
  * Bump Build-Depends on libhtml-mason-perl to (>= 1:1.36-2) to use the fix
    for #434344 (missing dependency on libhtml-parser-perl)
  * Move libmodule-build-perl from B-D-Indep to B-D, since it is used in the
    clean target. Thanks, lintian.

 -- Niko Tyni <ntyni@iki.fi>  Tue, 24 Jul 2007 21:13:41 +0300

libmasonx-interp-withcallbacks-perl (1.16-1) unstable; urgency=low

  * New upstream release.
  * Add libtest-pod-perl to Build-Depends-Indep.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Mon,  7 Aug 2006 01:19:34 +0200

libmasonx-interp-withcallbacks-perl (1.15-1) unstable; urgency=low

  * New upstream release.
  * Set Debhelper Compatibility Level to 5.
  * Set Standards-Version to 3.7.2 (no changes).
  * Specified versions in Build-Dependencies.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sat, 17 Jun 2006 17:38:04 +0200

libmasonx-interp-withcallbacks-perl (1.13-3) unstable; urgency=low

  * New Maintainer (Closes: #348952)
  * debian/copyright: added url to homepage
  * debian/rules: clean up
  * added debian/watch file
  * Bumped up to 3.7.0 Standards Version
  * Moved debhelper depend to Build-Depend

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Thu, 20 Apr 2006 13:47:12 +0200

libmasonx-interp-withcallbacks-perl (1.13-2) unstable; urgency=low

  * Orphan package.

 -- Clint Adams <schizo@debian.org>  Sat, 21 Jan 2006 18:54:01 -0500

libmasonx-interp-withcallbacks-perl (1.13-1) unstable; urgency=low

  * Initial Release.

 -- Clint Adams <schizo@debian.org>  Tue, 13 Dec 2005 11:48:08 -0500
